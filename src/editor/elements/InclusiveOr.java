package editor.elements;

/**
 *
 * @author Skorikov
 */
public class InclusiveOr extends Element {

    public InclusiveOr() {
    }

    public InclusiveOr(String style, int width, int height, Object value) {
        super(style, width, height, value);
    }
}
