package editor.elements;

/**
 *
 * @author Skorikov
 */
public class ExclusiveOr extends Element {

    public ExclusiveOr() {
    }

    public ExclusiveOr(String style, int width, int height, Object value) {
        super(style, width, height, value);
    }
}
