package editor.elements;

/**
 *
 * @author Skorikov
 */
public class And extends Element {

    public And() {
    }

    public And(String style, int width, int height, Object value) {
        super(style, width, height, value);
    }
}
