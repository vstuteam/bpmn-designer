package editor.elements;

/**
 *
 * @author Skorikov
 */
public class EndErrorEvent extends Element {

    public EndErrorEvent() {
    }

    public EndErrorEvent(String style, int width, int height, Object value) {
        super(style, width, height, value);
    }
}
