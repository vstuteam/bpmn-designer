package editor.elements;

/**
 *
 * @author Skorikov
 */
public class ClosedPool extends Element {
    
    public ClosedPool() {
    }
    
    public ClosedPool(String style, int width, int height, Object value) {
        super(style, width, height, value);
    }
}
