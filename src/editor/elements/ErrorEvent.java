package editor.elements;

/**
 *
 * @author Skorikov
 */
public class ErrorEvent extends Element {

    public ErrorEvent() {
    }

    public ErrorEvent(String style, int width, int height, Object value) {
        super(style, width, height, value);
    }
}