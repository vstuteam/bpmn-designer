package editor.elements;

/**
 *
 * @author Skorikov
 */
public class StartEvent extends Element {

    public StartEvent() {
    }

    public StartEvent(String style, int width, int height, Object value) {
        super(style, width, height, value);
    }
}
