package editor.elements;

/**
 *
 * @author Skorikov
 */
public class EndEvent extends Element {

    public EndEvent() {
    }

    public EndEvent(String style, int width, int height, Object value) {
        super(style, width, height, value);
    }
}
