package editor.elements;

/**
 *
 * @author Skorikov
 */
public class AdHoc extends Element {

    public AdHoc() {
    }

    public AdHoc(String style, int width, int height, Object value) {
        super(style, width, height, value);
    }
}
