import com.mxgraph.canvas.mxGraphics2DCanvas;
import com.mxgraph.canvas.mxGraphicsCanvas2D;
import com.mxgraph.shape.mxSwimlaneShape;
import com.mxgraph.util.mxConstants;
import com.mxgraph.util.mxUtils;
import com.mxgraph.view.mxCellState;

/**
 * Created by ShadowGorn on 04.11.2015.
 */

public class AdHocShape extends mxSwimlaneShape {
    public AdHocShape() {
    }

    protected void paintRoundedSwimlane(mxGraphics2DCanvas var1, mxCellState var2, double var3, double var5, String var7, boolean var8) {
        mxGraphicsCanvas2D var9 = this.configureCanvas(var1, var2, new mxGraphicsCanvas2D(var1.getGraphics()));
        double var10 = var2.getWidth();
        double var12 = var2.getHeight();
        if(!mxConstants.NONE.equals(var7)) {
            var9.save();
            var9.setFillColor(var7);
            var9.roundrect(0.0D, 0.0D, var10, var12, var5, var5);
            var9.fillAndStroke();
            var9.restore();
            var9.setShadow(false);
        }

        var9.begin();
        if(mxUtils.isTrue(var2.getStyle(), mxConstants.STYLE_HORIZONTAL, true)) {
            if(var8 || var3 >= var12) {
                var9.moveTo(var10, var3/2.0D);
                var9.lineTo(var10, var5);
                var9.quadTo(var10, 0.0D, var10 - Math.min(var10 / 2.0D, var5), 0.0D);
                var9.lineTo(Math.min(var10 / 2.0D, var5), 0.0D);
                var9.quadTo(0.0D, 0.0D, 0.0D, var5);
                var9.lineTo(0.0D, var3 - var5);
                var9.quadTo(0.0D, var3, Math.min(var10 / 2.0D, var5), var3);
                var9.lineTo(var10 - Math.min(var10 / 2.0D, var5), var3);
                var9.quadTo(var10, var3, var10, var3 - var5);
                var9.close();
            } else {
                var9.moveTo(var10, var3);
                var9.lineTo(var10, var5);
                var9.quadTo(var10, 0.0D, var10 - Math.min(var10 / 2.0D, var5), 0.0D);
                var9.lineTo(Math.min(var10 / 2.0D, var5), 0.0D);
                var9.quadTo(0.0D, 0.0D, 0.0D, var5);
                var9.lineTo(0.0D, var3);
            }

            var9.setFontSize(20.0D);
            var9.plainText(var10/2.0D,var12-20,20,20,"~","center","center",true,null,null,false,0.0D);

            var9.fillAndStroke();
            if(var3 < var12 && mxConstants.NONE.equals(var7)) {
                var9.begin();
                var9.moveTo(0.0D, var3);
                var9.lineTo(0.0D, var12 - var5);
                var9.quadTo(0.0D, var12, Math.min(var10 / 2.0D, var5), var12);
                var9.lineTo(var10 - Math.min(var10 / 2.0D, var5), var12);
                var9.quadTo(var10, var12, var10, var12 - var5);
                var9.lineTo(var10, var3);
                var9.stroke();
            }
        } else {
            var9.moveTo(var3, 0.0D);
            var9.lineTo(var5, 0.0D);
            var9.quadTo(0.0D, 0.0D, 0.0D, Math.min(var12 / 2.0D, var5));
            var9.lineTo(0.0D, var12 - Math.min(var12 / 2.0D, var5));
            var9.quadTo(0.0D, var12, var5, var12);
            var9.lineTo(var3, var12);
            if(var8 || var3 >= var10) {
                var9.close();
            }

            var9.fillAndStroke();
            if(var3 < var10 && mxConstants.NONE.equals(var7)) {
                var9.begin();
                var9.moveTo(var3, var12);
                var9.lineTo(var10 - var5, var12);
                var9.quadTo(var10, var12, var10, var12 - Math.min(var12 / 2.0D, var5));
                var9.lineTo(var10, Math.min(var12 / 2.0D, var5));
                var9.quadTo(var10, 0.0D, var10 - var5, 0.0D);
                var9.lineTo(var3, 0.0D);
                var9.stroke();
            }
        }

    }
}
