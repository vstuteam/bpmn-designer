import com.mxgraph.canvas.mxGraphics2DCanvas;
import com.mxgraph.io.mxCodec;
import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGeometry;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.swing.util.mxSwingConstants;
import com.mxgraph.util.mxConstants;
import com.mxgraph.util.mxRectangle;
import com.mxgraph.util.mxResources;
import com.mxgraph.util.mxUtils;
import com.mxgraph.view.mxCellState;
import com.mxgraph.view.mxGraph;
import editor.BasicGraphEditor;
import editor.EditorPalette;
import editor.elements.AdHoc;
import editor.elements.And;
import editor.elements.ClosedPool;
import editor.elements.EndEvent;
import editor.elements.EndErrorEvent;
import editor.elements.ErrorEvent;
import editor.elements.ExclusiveOr;
import editor.elements.InclusiveOr;
import editor.elements.StartEvent;
import editor.elements.Task;
import org.w3c.dom.Document;

import javax.swing.*;
import java.awt.*;
import java.util.Map;

public class GraphEditor extends BasicGraphEditor {

    public GraphEditor() {
        this("mxGraph Editor", new CustomGraphComponent(new CustomGraph()));
    }

    /**
     *
     */
    public GraphEditor(String appTitle, mxGraphComponent component) {
        super(appTitle, component);

        // Creates the shapes palette
        EditorPalette shapesPalette = insertPalette(mxResources.get("shapes"));

        // Adds some template cells for dropping into the graph
        shapesPalette.addTemplate(
                "Задание",
                new ImageIcon(
                        GraphEditor.class
                                .getResource("/images/rounded.png")),
                "rounded=1", 100, 60, "Задание");
        shapesPalette
                .addTemplate(
                        "Закрытый пул",
                        new ImageIcon(
                                GraphEditor.class
                                        .getResource("/images/rectangle.png")),
                null, 200, 80, "Закрытый пул");
        shapesPalette
                .addTemplate(
                        "Стартовое событие",
                        new ImageIcon(
                                GraphEditor.class
                                        .getResource("/images/event.png")),
                "roundImage;image=/images/event.png",50, 50, "");
        shapesPalette
                .addTemplate(
                        "Конечное событие",
                        new ImageIcon(
                                GraphEditor.class
                                        .getResource("/images/event_end.png")),
                "roundImage;image=/images/event_end.png",50, 50, "");
        shapesPalette
                .addTemplate(
                        "Исключающее ИЛИ",
                        new ImageIcon(
                                GraphEditor.class
                                        .getResource("/images/rhombus.png")),
                "rhombus", 50, 50, "");
        shapesPalette
                .addTemplate(
                        "Включающее ИЛИ",
                        new ImageIcon(
                                GraphEditor.class
                                        .getResource("/images/inclusive.png")),
                "rhombusImage;image=/images/inclusive.png",50, 50, "");
        shapesPalette
                .addTemplate(
                        "И",
                        new ImageIcon(
                                GraphEditor.class
                                        .getResource("/images/fork.png")),
                "rhombusImage;image=/images/fork.png",50, 50, "");
        shapesPalette
                .addTemplate(
                        "Событие-ошибка",
                        new ImageIcon(
                                GraphEditor.class
                                        .getResource("/images/error.png")),
                "roundImage;image=/images/error.png",50, 50, "");
        shapesPalette
                .addTemplate(
                        "Конечное событие-ошибка",
                        new ImageIcon(
                                GraphEditor.class
                                        .getResource("/images/error_end.png")),
                "roundImage;image=/images/error_end.png",50, 50, "");
        shapesPalette
                .addTemplate(
                        "Подпроцесс",
                        new ImageIcon(
                                GraphEditor.class
                                        .getResource("/images/adhoc.png")),
                "adhoc;verticalAlign=top;swimlaneLine=false;spacingBottom=20", 280, 280, "Ad-hoc подпроцесс");
    }

    /**
     *
     */
    public static class CustomGraphComponent extends mxGraphComponent {
        /**
         * @param graph
         */
        public CustomGraphComponent(mxGraph graph) {
            super(graph);

            // Sets switches typically used in an editor
            setPageVisible(false);
            setGridVisible(true);
            setToolTips(true);
            getConnectionHandler().setCreateTarget(false);

            // Loads the defalt stylesheet from an external file
            mxCodec codec = new mxCodec();
            Document doc = mxUtils.loadDocument(GraphEditor.class.getResource(
                    "/resources/default-style.xml")
                    .toString());
            codec.decode(doc.getDocumentElement(), graph.getStylesheet());

            // Sets the background to white
            getViewport().setOpaque(true);
            getViewport().setBackground(Color.WHITE);
        }
    }

    /**
     * A graph that creates new edges from a given template edge.
     */
    public static class CustomGraph extends mxGraph {
        /**
         * Custom graph that defines the alternate edge style to be used when
         * the middle control point of edges is double clicked (flipped).
         */
        public CustomGraph() {
            setAlternateEdgeStyle("edgeStyle=mxEdgeStyle.ElbowConnector;elbow=vertical");
            setAutoOrigin(true);
        }

        public boolean isSwimlane(Object var1) {
            if(var1 != null && this.model.getParent(var1) != this.model.getRoot()) {
                mxCellState var2 = this.view.getState(var1);
                Map var3 = var2 != null?var2.getStyle():this.getCellStyle(var1);
                if(var3 != null && !this.model.isEdge(var1)) {
                    return mxUtils.getString(var3, mxConstants.STYLE_SHAPE, "").equals("swimlane") ||
                            mxUtils.getString(var3, mxConstants.STYLE_SHAPE, "").equals("adhoc");
                }
            }

            return false;
        }

        public void updateAlternateBounds(Object var1, mxGeometry var2, boolean var3) {
            if(var1 != null && var2 != null) {
                if(var2.getAlternateBounds() == null) {
                    Object var4 = null;
                    if(this.isCollapseToPreferredSize()) {
                        var4 = this.getPreferredSizeForCell(var1);
                        if(this.isSwimlane(var1)) {
                            mxRectangle var5 = this.getStartSize(var1);
                            ((mxRectangle)var4).setHeight(Math.max(((mxRectangle)var4).getHeight(), var5.getHeight()));
                            ((mxRectangle) var4).setWidth(var2.getWidth());
                        }
                    }

                    if(var4 == null) {
                        var4 = var2;
                    }

                    var2.setAlternateBounds(new mxRectangle(var2.getX(), var2.getY(), ((mxRectangle)var4).getWidth(), ((mxRectangle)var4).getHeight()));
                } else {
                    var2.getAlternateBounds().setX(var2.getX());
                    var2.getAlternateBounds().setY(var2.getY());
                }
            }

        }

        public mxRectangle getCellContainmentArea(Object var1) {
            if(var1 != null && !this.model.isEdge(var1)) {
                Object var2 = this.model.getParent(var1);
                if(var2 == this.getDefaultParent() || var2 == this.getCurrentRoot()) {
                    return this.getMaximumGraphBounds();
                }

                if(var2 != null && var2 != this.getDefaultParent()) {
                    mxGeometry var3 = this.model.getGeometry(var2);
                    if(var3 != null) {
                        double var4 = 0.0D;
                        double var6 = 0.0D;
                        double var8 = var3.getWidth();
                        double var10 = var3.getHeight();
                        if(this.isSwimlane(var2)) {
                            mxRectangle var12 = this.getStartSize(var2);
                            var4 = var12.getWidth();
                            var8 -= var12.getWidth();
                            var6 = var12.getHeight();
                            var10 -= var12.getHeight();

                            Map var11 = this.getCellStyle(var2);
                            var10 -= mxUtils.getDouble(var11, mxConstants.STYLE_SPACING_BOTTOM);
                        }

                        return new mxRectangle(var4, var6, var8, var10);
                    }
                }
            }

            return null;
        }

        /**
         * Prints out some useful information about the cell in the tooltip.
         */
        public String getToolTipForCell(Object cell) {
            String tip = "<html>";

            if (getModel().isVertex(cell)) {
                tip += ((mxCell) cell).getValue();
            }

            tip += "</html>";

            return tip;
        }

        /**
         * Overrides the method to use the currently selected edge template for
         * new edges.
         *
         * @param parent
         * @param id
         * @param value
         * @param source
         * @param target
         * @param style
         * @return
         */
        public Object createEdge(Object parent, String id, Object value,
                                 Object source, Object target, String style) {
            return super.createEdge(parent, id, value, source, target, style);
        }
        
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        mxGraphics2DCanvas.putShape("adhoc", new AdHocShape());

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        mxSwingConstants.SHADOW_COLOR = Color.WHITE;
        mxConstants.W3C_SHADOWCOLOR = "#ffffff";

        GraphEditor editor = new GraphEditor();
        editor.createFrame(new JMenuBar()).setVisible(true);
    }
}
